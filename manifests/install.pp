# @summary A short summary of the purpose of this class
#
# A description of what this class does
#
# @example
#   include sublime_text::install
class sublime_text::install {

  case $sublime_text::package_ensure {
		'present', 'installed', 'latest', 'held': {
      include '::apt'

      create_resources('::apt::source', $sublime_text::sources)
      Class['apt::update'] -> Package <| |>
		}
	}

  package { $sublime_text::package_name:
    ensure => $sublime_text::package_ensure,
  }
}
