# @summary A short summary of the purpose of this class
#
# A description of what this class does
#
# @example
#   include sublime_text
class sublime_text (
  String        $package_ensure,
  Array[String] $package_name,
  Hash          $sources,
) {
  contain 'sublime_text::install'
}
